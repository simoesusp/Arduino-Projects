/*  MonsterMoto Shield Example Sketch
  date: 5/24/11
  code by: Jim Lindblom
  hardware by: Nate Bernstein
  SparkFun Electronics
 This is really simple example code to get you some basic
 functionality with the MonsterMoto Shield. The MonsterMote uses
 two VNH2SP30 high-current full-bridge motor drivers.
 
 Use the motorGo(uint8_t motor, uint8_t direct, uint8_t pwm) 
 function to get motors going in either CW, CCW, BRAKEVCC, or 
 BRAKEGND. Use motorOff(int motor) to turn a specific motor off.
 
 The motor variable in each function should be either a 0 or a 1.
 pwm in the motorGo function should be a value between 0 and 255.
 
This code is beerware; if you see me (or any other SparkFun employee) at the
local, and you've found our code helpful, please buy us a round!
Distributed as-is; no warranty is given.
 */
#define pwm1 5
#define pwm2 6
#define dir1A 7
#define dir1B 8
#define dir2A 4
#define dir2B 9
#define led 13

int count = 0;



void setup()
{
  Serial.begin(9600);
  
    pinMode(led, OUTPUT);
    pinMode(pwm1, OUTPUT);
    pinMode(pwm2, OUTPUT);
    pinMode(dir1A, OUTPUT);
    pinMode(dir1B,OUTPUT);
    pinMode(dir2A, OUTPUT);
    pinMode(dir2B,OUTPUT);
  
    digitalWrite(dir1A, LOW);
    digitalWrite(dir1B, HIGH);
    digitalWrite(dir2A, LOW);
    digitalWrite(dir2B, HIGH);  
    digitalWrite(led,   HIGH); 

     analogWrite(pwm1, 1023);
     analogWrite(pwm2, 1023);
}

void loop()
{

count ++;
if (count == 1023){
   count =0;
}

     analogWrite(pwm1, count);
     analogWrite(pwm2, count);

delay(10);
  
 
}
